package org.example;


import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class StringManagerTest {

    private final StringManager stringManager = new StringManager();

    @Test
    public void getReverseUppercaseString() {
        String source = "Hello world!";
        String expected = "!DLROW OLLEH";

        String actual = stringManager.getReverseUppercaseString(source);

        assertEquals(expected, actual);
    }

}